name := "very-util-db-mongo"

version := "0.0.0"

scalaVersion := "2.11.8"

resolvers ++= Seq(
"Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
"Sonatype releases" at "https://oss.sonatype.org/content/repositories/releases",
 "scalaTools Archive" at "https://oss.sonatype.org/content/groups/scala-tools",
 "Maven.Org Repository" at "http://repo1.maven.org/maven2/org",
	"cicamaven" at "http://ftp.cica.es/mirrors/maven2/",
	"oschina maven" at "http://maven.oschina.net/content/groups/public/",
	"oschina maven thirdparty" at "http://maven.oschina.net/content/repositories/thirdparty/",
  "Typesafe repository releases" at "http://repo.typesafe.com/typesafe/releases/",
  "Scaladin Snapshots" at "http://henrikerola.github.io/repository/snapshots/",
  "Vaadin addons" at "http://maven.vaadin.com/vaadin-addons"
)

lazy val lang = RootProject(file("../very-util-lang"))

lazy val config = RootProject(file("../very-util-config"))

libraryDependencies += "org.mongodb" % "mongo-java-driver" % "3.3.0"



lazy val mongo = project.in(file(".")).dependsOn(lang,config)
